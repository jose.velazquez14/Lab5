package menuClasses;

import dataManager.DMComponent;
import ioManagementClasses.IOComponent;

public class DeleteFromListAction implements Action {

	@Override
	public void execute(Object args) {
		DMComponent dm = (DMComponent) args; 
		IOComponent io = IOComponent.getComponent(); 
		io.output("\nRemoving a list of Integer values from the system:\n"); 
		String listName = io.getInput("\nEnter name of the list: "); 
		
//		for(int i =0; i<listName.length();i++){
//			dm.removeElementFromList(listName, i);
//		}
		 
		dm.removeList(listName);
		
		
	}

}
